# Java Kata Starter

Java project for quick kata setup. 
The `master` branch hosts the latest java version. 
A branch will be created for older versions. 

For now the current Java version is Java 17. 

## Dependencies
The following dependencies have been added : 
* [junit 5](https://junit.org/junit5/docs/current/user-guide/)
* [mockito](https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html)
* [approvals](https://github.com/approvals/ApprovalTests.Java)
* [pitest](https://pitest.org/) (launch `gradle pitest` to generate the report)