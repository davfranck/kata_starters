package eu.davidfranck;

record Foo(String value) {

    public String saySomething() {
        return "sayIt";
    }
}
