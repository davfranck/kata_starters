package eu.davidfranck;

import org.approvaltests.Approvals;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

public class FooTest {

    @Test
    void assertJ_is_ok() {
        Assertions.assertThat(new Foo("Foo")).isEqualTo(new Foo("Foo"));
    }

    @Test
    void mockito_is_ok() {
        Bar crazyMock = Mockito.mock(Bar.class);
        BDDMockito.given(crazyMock.toLowerCase()).willReturn("ALWAYS THE SAME");

        String toLowerCaseResult = crazyMock.toLowerCase();

        Assertions.assertThat(toLowerCaseResult).isEqualTo("ALWAYS THE SAME");
    }

    @Test
    void approvals_is_ok() {
        Approvals.verify("Hello");
    }

    static class Bar {
        String toLowerCase(){
            return "ANYTHING";
        }
    }

}
