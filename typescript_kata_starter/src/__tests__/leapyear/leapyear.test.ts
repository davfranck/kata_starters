import { isLeapYear } from "../../main/leapyear/leapryear"

test('does it work ?', () => {
    expect(isLeapYear(2000)).toBeTruthy()
})