module.exports = {
    "roots": [
        // tous les fichiers typescript sont dans src
        "<rootDir>/src"
    ],
    // patterns pour trouver les fichiers de test
    "testMatch": [
        "**/__tests__/**/*.+(ts|tsx|js)",
        "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    // pour la tranformation des fichiers ts afin de pouvoir les exécuter
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
}